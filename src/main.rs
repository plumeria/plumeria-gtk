#![deny(rust_2018_idioms)]

use gtk::{BoxExt, ContainerExt, EntryExt, GtkWindowExt, HeaderBarExt, PanedExt, WidgetExt};

fn main() {
    gtk::init().expect("Failed to initalize GTK");

    let paned = gtk::Paned::new(gtk::Orientation::Horizontal);

    let editor = editor();
    paned.add1(&editor);

    let context = context();
    paned.add2(&context);

    let window = window();
    window.add(&paned);
    window.show_all();

    gtk::main();
}

fn window() -> gtk::Window {
    let window = gtk::Window::new(gtk::WindowType::Toplevel);

    window.set_title("Plumeria");
    window.set_position(gtk::WindowPosition::Center);
    window.set_default_size(300, 300);

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        gtk::Inhibit(false)
    });

    let header = gtk::HeaderBar::new();
    header.set_title("Plumeria");
    header.set_subtitle("^^");
    header.set_show_close_button(true);

    window.set_titlebar(&header);
    window
}

fn editor() -> gtk::Box {
    let gbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
    let header = gtk::HeaderBar::new();
    header.set_title("Editor");
    gbox.pack_start(&header, false, false, 0);

    let listbox = gtk::ListBox::new();

    for _ in 0..3 {
        let item = item();
        listbox.add(&item);
    }

    gbox.pack_start(&listbox, true, true, 0);
    gbox
}

fn item() -> gtk::Box {
    let item = gtk::Box::new(gtk::Orientation::Vertical, 4);

    let top = gtk::Box::new(gtk::Orientation::Horizontal, 14);
    let label = gtk::Label::new("things");
    top.pack_start(&label, false, false, 14);
    ["a", "b", "c"].iter().for_each(|label| {
        let button = gtk::Button::new_with_label(label);
        top.pack_start(&button, false, false, 1);
    });

    let bottom = gtk::Box::new(gtk::Orientation::Horizontal, 14);
    let attribute = gtk::Entry::new();
    attribute.set_placeholder_text("attributes");
    bottom.pack_start(&attribute, true, true, 14);

    item.pack_start(&top, true, true, 4);
    item.pack_start(&bottom, true, true, 4);
    item
}

fn context() -> gtk::Box {
    let gbox = gtk::Box::new(gtk::Orientation::Vertical, 0);

    let header = gtk::HeaderBar::new();
    header.set_title("Context");
    gbox.pack_start(&header, false, false, 0);

    let label = gtk::Label::new("where things appear and whatnot");
    gbox.pack_start(&label, true, true, 0);

    gbox
}
